package br.org.repiboleto.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public final class EmailService {
	private Properties emailProps;
	private String host;
	private String smtpPort;
	private String remetente;
	private String emailRemetente;
	private String senha;
	private String assunto;
	private String mensagem;	
	private String ssl;
	private static EmailService instance;
		
	private EmailService() throws IOException {
		emailProps = new Properties();
		try {
			emailProps.load(new FileInputStream("email.properties"));
			loadProperties();
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("Arquivo de propriedades n�o encontrado!");
		} catch (IOException e) {
			throw new IOException("Erro ao ler arquivo de propriedades!");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void loadProperties() throws IllegalArgumentException, IllegalAccessException {
		Class<?> classe = this.getClass();
		for (Field field : classe.getDeclaredFields()) {
			if (!field.getName().equals("emailProps"))
				field.set(this, emailProps.getProperty(field.getName()));
		}		
	}
	
	public static EmailService getInstance() throws IOException {
		if (instance == null)
			instance = new EmailService();
		return instance;
	}
	
	public void enviarEmail(Morador morador, File boleto) throws EmailException {
		// Create the attachment
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath(boleto.getAbsolutePath());
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		attachment.setDescription("Boleto de pagamento da taxa");
		attachment.setName("boleto.pdf");

		
		// Create the email message
		MultiPartEmail email = new MultiPartEmail();
		email.setDebug(true);
		email.setStartTLSEnabled(true);
		email.setSmtpPort(Integer.parseInt(smtpPort));
		email.setAuthentication(emailRemetente, senha);
		email.setSSLCheckServerIdentity(Boolean.parseBoolean(ssl));
		email.setHostName(host);
		
		try{
		email.addTo(morador.getEmail(), morador.getNome()); 
		} catch (EmailException e) {
			throw new EmailException("Destinat�rio do e-mail nulo!");
		}
		
		try {
			email.setFrom(emailRemetente, remetente);
		} catch(EmailException e) {
			throw new EmailException("Remetente do e-mail nulo!");
		}
		
		email.setSubject(assunto);
		
		try {
			email.setMsg(mensagem);
		} catch (EmailException e) {
			throw new EmailException("Corpo de e-mail nulo!");
		}
		
		try{
			// add the attachment
			email.attach(attachment);
		} catch(EmailException e) {
			throw new EmailException("Erro em anexo do e-mail!");
		}

		try {
			// send the email
			email.send();
		} catch(EmailException e) {
			throw new EmailException("Erro ao enviar e-mail! " + e.getMessage());
		}		
	}
}