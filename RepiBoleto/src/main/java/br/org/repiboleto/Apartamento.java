package br.org.repiboleto;

public class Apartamento implements Comparable<Apartamento> {
	private int numero;
	private int andar;
	private volatile int hashCode;
	public Apartamento(final int numero, final int andar) {
		if (numero == 0)
			throw new IllegalArgumentException("Numero n�o deve ser nulo");
		
		if (andar == 0)
			throw new IllegalArgumentException("Andar n�o deve ser nulo");
		
		this.numero = numero;
		this.andar = andar;
	}

	public int getNumero() {
		return numero;
	}

	public int getAndar() {
		return andar;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void setAndar(int andar) {
		this.andar = andar;
	}
	
	@Override
	public String toString() {
		return String.format("[Andar %d Num. %d]", andar, numero);
	}

	@Override
	public int hashCode() {
		int result = hashCode;
		if (hashCode == 0) {
			result = 17;
			result = 31 * result + andar;
			result = 31 * result + numero;
			hashCode = result;
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Apartamento other = (Apartamento) obj;
		if (andar != other.andar)
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}

	@Override
	public int compareTo(Apartamento ap) {
		int andarDiff = andar - ap.andar;
		if (andarDiff != 0)
			return andarDiff;
		return numero - ap.numero;
	}
}