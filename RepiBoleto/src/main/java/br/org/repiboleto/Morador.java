package br.org.repiboleto;

import br.org.repiboleto.ui.Coluna;

public final class Morador implements Comparable<Morador> {
	private String nome;
	private String cpf;
	private String email;
	private String telefone;
	private String matricula;
	private Apartamento apartamento;
	private boolean hospede = false;
	private volatile int hashCode;
		
	public Morador(final String nome, final String cpf, final String email,
			final String telefone, final String matricula, final boolean hospede,
			final Apartamento apartamento) {
		if (nome == null || nome.equals(""))
			throw new IllegalArgumentException("Morador deve ter um nome");

		if (matricula == null || nome.equals(""))
			throw new IllegalArgumentException(
					"N�mero da matr�cula n�o pode ser nulo");

		if (apartamento == null)
			throw new IllegalArgumentException("Apartamento n�o pode ser nulo");

		if (email == null || email.equals(""))
			throw new IllegalArgumentException("Email n�o pode ser nulo");

		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.telefone = telefone;
		this.matricula = matricula;
		if (hospede)
			this.hospede = true;
		this.apartamento = apartamento;
	}

	@Coluna(nome="Nome", posicao=0)
	public String getNome() {
		return nome;
	}
	
	@Coluna(nome="CPF", posicao=1)
	public String getCpf() {
		return cpf;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public String getMatricula() {
		return matricula;
	}
	
	public Apartamento getApartamento() {
		return apartamento;
	}
	
	public boolean isHospede() {
		return hospede;
	}
	
	@Coluna(nome="N�mero AP.", posicao=2)
	public String getNumAP() {
		return String.valueOf(getApartamento().getNumero());
	}
	
	@Coluna(nome="Andar", posicao=3)
	public String getAndar() {
		return String.valueOf(getApartamento().getAndar());
	}
	
	public void setNome(final String nome) {
		this.nome = nome;
	}

	public void setCpf(final String cpf) {
		this.cpf = cpf;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setTelefone(final String telefone) {
		this.telefone = telefone;
	}
	
	public void setMatricula(final String matricula) {
		this.matricula = matricula;
	}
	
	public void setHospede(final boolean hospede) {
		this.hospede = hospede;
	}

	@Override
	public String toString() {
		return String.format("%s, Ap. %d%d", nome, apartamento.getNumero(), apartamento.getAndar());
	}

	@Override
	public int hashCode() {
		int result = hashCode;
		if (result == 0) {
			final int prime = 31;
			result = 17;
			result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + ((nome == null) ? 0 : nome.hashCode());
			result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
			result = prime * result + (hospede ? 1231 : 1237);
			result = prime * result
					+ ((telefone == null) ? 0 : telefone.hashCode());
			hashCode = result;
		}		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Morador other = (Morador) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (hospede != other.hospede)
			return false;
		if (apartamento == null) {
			if (other.apartamento != null)
				return false;
		} else if (! apartamento.equals(other.apartamento))
			return false;
		
		return true;
	}

	@Override
	public int compareTo(Morador morador) {
		int nomeDiff = String.CASE_INSENSITIVE_ORDER.compare(nome, morador.nome);
		if (nomeDiff != 0)
			return nomeDiff;
		int cpfDiff = String.CASE_INSENSITIVE_ORDER.compare(cpf, morador.cpf);
		if (cpfDiff != 0)
			return cpfDiff;
		int emailDiff = String.CASE_INSENSITIVE_ORDER.compare(email, morador.email);
		if (emailDiff != 0)
			return emailDiff;
		int telefoneDiff = String.CASE_INSENSITIVE_ORDER.compare(telefone, morador.telefone);
		if (telefoneDiff != 0)
			return telefoneDiff;
		
		return apartamento.compareTo(morador.apartamento);
	}
}