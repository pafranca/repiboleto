package br.org.repiboleto.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LimparAction extends DefaultRepiBoletoActionUI {
	
	public LimparAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		cleanFields();
		if (repiBoleto.index != -1)
			repiBoleto.index = -1;
		
		if (repiBoleto.selectedModel != null) {
			repiBoleto.selectedModel = null;
			repiBoleto.atm.fireTableDataChanged();
		}
		return;
	}

}
