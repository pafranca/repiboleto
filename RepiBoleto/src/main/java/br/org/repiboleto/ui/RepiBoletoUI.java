package br.org.repiboleto.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import br.org.repiboleto.Morador;
import br.org.repiboleto.reader.RepositorioMorador;

public class RepiBoletoUI extends JFrame {
	private JPanel painelPrincipal, painelDoCentro, painelDaTabela, painelBotoes;
	protected JTextField campoNome, campoCpf, campoEmail, campoTelefone, campoMatricula, campoNumeroAp, campoAndar;
	protected JCheckBox campoHospede;
	protected JTable tabela;
	private JScrollPane scroll;
	private JPanel painelForm;
	private RepositorioMorador repositorioMorador;
	protected List<Morador> moradores;
	protected RepiBoletoTableModel atm;
	protected int index;
	protected Morador selectedModel;
						
	public static void main(String[] args) {
		Locale.setDefault(new Locale("pt", "BR"));
		new RepiBoletoUI();
	}
	
	public RepiBoletoUI() {
		repositorioMorador = new RepositorioMorador();
		montaTela();
	}

	private void montaTela() {
		preparaJanela();
		preparaPainelPrincipal();
		preparaTitulo();
		preparaPainelDoCentro();
		preparaCampos();
		preparaTabelaDados();
		preparaPainelBotoesCrud();
		preparaBotaoEmitirBoleto();
		preparaBotaoEmitirTodosBoleto();
		carregaDados();
		preparaTamanhoColunas();
		preparaSelecaoTabela();
		preparaBotaoSair();
		preparaCloseJFrameListener();
		
		mostraJanela();
	}
	
	private void preparaJanela() {
		this.setTitle("RepiBoleto");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	private void preparaPainelPrincipal() {
		painelPrincipal = new JPanel();
		painelPrincipal.setLayout(new BorderLayout());
		//painelPrincipal.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.white));
		this.add(painelPrincipal);
	}
	
	private void preparaTitulo() {
		JLabel titulo = new JLabel("RepiBoleto", SwingConstants.CENTER);
		
		titulo.setFont(new Font("Verdana", Font.BOLD, 35));
		//titulo.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.white));
		painelPrincipal.add(titulo, BorderLayout.NORTH);
	}
	
	private void preparaPainelDoCentro() {
		painelDoCentro = new JPanel();
		painelDoCentro.setLayout(new BorderLayout());
		//painelDoCentro.setBorder(BorderFactory.createEtchedBorder(Color.red, Color.white));
		//painelDoCentro.setBounds(1, 1, 400, 600);
		
		painelPrincipal.add(painelDoCentro, BorderLayout.CENTER);
	}
	
	private void preparaCampos() {
		painelForm = new JPanel();
		painelForm.setLayout(new GridBagLayout());
		//painelForm.setBorder(BorderFactory.createEtchedBorder(Color.blue, Color.white));
		
		painelDoCentro.add(painelForm, BorderLayout.NORTH);
		
		FormUtilityGridBagLayout formUtility = new FormUtilityGridBagLayout();
		
		formUtility.addLabel("Nome: ", painelForm);
		campoNome = new JTextField();
		campoNome.setName("campoNome");
		formUtility.addLastField(campoNome, painelForm);
		
		formUtility.addLabel("E-mail: ", painelForm);
		campoEmail = new JTextField();
		campoEmail.setName("campoEmail");
		formUtility.addMiddleField(campoEmail, painelForm);
		
		JPanel docsPainel = new JPanel(new GridBagLayout());
		//cpfPainel.setSize(new Dimension(3, 1));
		//cpfPainel.setBounds(0, 0, 1, 1);
		//cpfPainel.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.white));
		
		formUtility.addLabel(" CPF: ", docsPainel);
		campoCpf = new JTextField();
		campoCpf.setName("campoCpf");
		Dimension campoCpfSize = campoCpf.getPreferredSize();
		campoCpfSize.width = 160;
		campoCpf.setPreferredSize(campoCpfSize);
		formUtility.addLabel(campoCpf, docsPainel);
		
		formUtility.addLabel(" Matr�cula: ", docsPainel);
		campoMatricula = new JTextField();
		campoMatricula.setName("campoMatricula");
		Dimension campoMatriculaSize = campoMatricula.getPreferredSize();
		campoMatriculaSize.width = 110;
		campoMatricula.setPreferredSize(campoMatriculaSize);
		formUtility.addLabel(campoMatricula, docsPainel);
		
		formUtility.addLastField(docsPainel, painelForm);
				
		formUtility.addLabel("Telefone: ", painelForm);
		campoTelefone = new JTextField();
		campoTelefone.setName("campoTelefone");
		formUtility.addMiddleField(campoTelefone, painelForm);
		
		JPanel apartPainel = new JPanel(new GridBagLayout());
		
		formUtility.addLabel(" N�mero Ap: ", apartPainel);
		campoNumeroAp = new JTextField();
		campoNumeroAp.setName("campoNumeroAp");
		Dimension campoNumeroSize = campoNumeroAp.getPreferredSize();
		campoNumeroSize.width = 60;
		campoNumeroAp.setPreferredSize(campoNumeroSize);
		formUtility.addLabel(campoNumeroAp, apartPainel);
		
		formUtility.addLabel(" Andar: ", apartPainel);
		campoAndar = new JTextField();
		campoAndar.setName("campoAndar");
		Dimension campoAndarSize = campoAndar.getPreferredSize();
		campoAndarSize.width = 70;
		campoAndar.setPreferredSize(campoAndarSize);
		formUtility.addLabel(campoAndar, apartPainel);
		
		formUtility.addLabel("  ", apartPainel);
		campoHospede = new JCheckBox("H�spede");
		campoHospede.setName("campoHospede");
		formUtility.addLabel(campoHospede, apartPainel);
		
		formUtility.addLabel(apartPainel, painelForm);
		formUtility.addLastField(new JPanel(), painelForm);
	}
	
	private void preparaTabelaDados() {
		painelDaTabela = new JPanel(new FlowLayout());
		//painelDaTabela.setBorder(BorderFactory.createEtchedBorder(Color.yellow, Color.white));
		painelDaTabela.setPreferredSize(new Dimension(834, 480));
		
		tabela = new JTable();
		tabela.setBorder(new LineBorder(Color.black));
		tabela.setGridColor(Color.black);
		tabela.setShowGrid(true);
		
		scroll = new JScrollPane();
		scroll.getViewport().setBorder(null);
		scroll.getViewport().add(tabela);
		scroll.setPreferredSize(new Dimension(828, 470));
		
		painelDaTabela.add(scroll);
		painelDoCentro.add(painelDaTabela);
	}
	
	private void preparaPainelBotoesCrud() {
		painelBotoes = new JPanel(new FlowLayout());
		painelDaTabela.add(painelBotoes, BorderLayout.SOUTH);
		
		preparaCrud();
	}
	
	private void preparaCrud() {
		preparaBotaoInserir();
		preparaBotaoCarregar();
		preparaBotaoAtualizar();
		preparaBotaoRemover();
	}
	
	private void preparaBotaoInserir() {
		JButton botaoInserir = new JButton("Inserir");
		botaoInserir.setPreferredSize(new Dimension(90, 40));
		botaoInserir.setMnemonic('i');
		botaoInserir.setName("botaoInserir");
		botaoInserir.addActionListener(new InserirAction(this));
		painelBotoes.add(botaoInserir);
	}
	
	private void preparaBotaoCarregar() {
		JButton botaoCarregar = new JButton("Limpar");
		botaoCarregar.setPreferredSize(new Dimension(90, 40));
		botaoCarregar.setMnemonic('l');
		botaoCarregar.setName("botaoCarregar");
		botaoCarregar.addActionListener(new LimparAction(this));
		painelBotoes.add(botaoCarregar);
	}
	
	private void preparaBotaoAtualizar() {
		JButton botaoAtualizar = new JButton("Atualizar");
		botaoAtualizar.setPreferredSize(new Dimension(100, 40));
		botaoAtualizar.setMnemonic('a');
		botaoAtualizar.setName("botaoAtualizar");
		botaoAtualizar.addActionListener(new AtualizarAction(this));
		painelBotoes.add(botaoAtualizar);
	}
	
	private void preparaBotaoRemover() {
		JButton botaoRemover = new JButton("Remover");
		botaoRemover.setPreferredSize(new Dimension(100, 40));
		botaoRemover.setMnemonic('r');
		botaoRemover.setName("botaoRemover");
		botaoRemover.addActionListener(new RemoverAction(this));
		painelBotoes.add(botaoRemover);
	}
	
	private void preparaBotaoEmitirBoleto() {
		JButton botaoBoleto = new JButton("Gerar Boleto");
		botaoBoleto.setPreferredSize(new Dimension(115, 40));
		botaoBoleto.setMnemonic('b');
		botaoBoleto.setName("botaoBoleto");
		botaoBoleto.addActionListener(new BoletoAction(this));
		painelBotoes.add(botaoBoleto);
	}
	
	private void preparaBotaoEmitirTodosBoleto() {
		JButton botaoTodosBoletos = new JButton("Gerar Todos Boletos");
		botaoTodosBoletos.setPreferredSize(new Dimension(170, 40));
		botaoTodosBoletos.setMnemonic('t');
		botaoTodosBoletos.setName("botaoTodosBoletos");
		botaoTodosBoletos.addActionListener(new TodosBoletosAction(this));
		painelBotoes.add(botaoTodosBoletos);
	}
	
	private void preparaBotaoSair() {
		JButton botaoSair = new JButton("Sair");
		botaoSair.setMnemonic('s');
		botaoSair.setName("botaoSair");
		botaoSair.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (moradores != null && moradores.size() != 0) {
					System.out.println("Salvando lista de moradores...");
					repositorioMorador.salvar(moradores);
				}
				System.out.println("Saindo do sistema...");
				System.exit(0);
			}
		});		
		painelPrincipal.add(botaoSair, BorderLayout.SOUTH);
	}
	
	private void carregaDados() {
		System.out.println("Verificando o arquivo XML com a lista de moradores.");
		FileReader arquivoDeMoradores = null;
		
		try {
			arquivoDeMoradores = carregaArquivo();
			System.out.println("Abrindo o arquivo XML com a lista de moradores.");
		} catch(FileNotFoundException e) {
			System.out.println("XML com moradores n�o encontrado");
			arquivoDeMoradores = null;
		}
		
		if (arquivoDeMoradores == null) {
			arquivoDeMoradores = criaArquivoVazio(arquivoDeMoradores);	
		}
		
		moradores = repositorioMorador.carregar(arquivoDeMoradores);
		Collections.sort(moradores);
		atm = new RepiBoletoTableModel(moradores);
		tabela.setModel(atm);		
	}
	
	private FileReader criaArquivoVazio(FileReader arquivoDeMoradores) {
		File arquivoNovo = new File("moradores.xml");
		try {
			arquivoNovo.createNewFile();
			System.out.println("Criando novo arquivo XML de moradores.");
			FileWriter out = new FileWriter(arquivoNovo);
			out.write("<list>" +
					"<morador>" +
					"<nome>Morador de exemplo</nome>" +
					"<cpf></cpf>" +
					"<email>exemplo@exemplo.com.br</email>" +
					"<telefone></telefone>" +
					"<matricula>9999999</matricula>" +
					"<apartamento><numero>0</numero></apartamento></morador>" +
					"</list>");
			out.close();
			
			arquivoDeMoradores = new FileReader(arquivoNovo);
		} catch (IOException e) {
			System.out.println("N�o foi poss�vel criar um novo arquivo.");
			JOptionPane.showMessageDialog(this, "N�o foi poss�vel iniciar a aplica��o\n" +
					"Verifique se existe permiss�o de escrita!", "Erro", JOptionPane.ERROR_MESSAGE);
		}
		return arquivoDeMoradores;
	}

	private FileReader carregaArquivo() throws FileNotFoundException {
		return new FileReader(new File("moradores.xml"));
	}
		
	private void preparaTamanhoColunas() {
		tabela.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabela.getColumnModel().getColumn(0).setPreferredWidth(440);
		tabela.getColumnModel().getColumn(1).setPreferredWidth(200);
		tabela.getColumnModel().getColumn(2).setPreferredWidth(100);
		tabela.getColumnModel().getColumn(3).setPreferredWidth(80);
	}

	private void preparaSelecaoTabela() {
		ListSelectionModel rowSelectionModel = tabela.getSelectionModel();
		rowSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setSelectedModel();
				//System.out.println(selectedModel);
				//System.out.println(index);
			}
		});
		index = -1;
	}
	
	private void setSelectedModel() {
		index = tabela.getSelectedRow();
		if (index != -1) {
			selectedModel = moradores.get(index);
			carregarRegistro();
		}
	}
	
	private void carregarRegistro() {
		campoNome.setText(selectedModel.getNome());
		campoCpf.setText(selectedModel.getCpf());
		campoEmail.setText(selectedModel.getEmail());
		campoTelefone.setText(selectedModel.getTelefone());
		campoMatricula.setText(selectedModel.getMatricula());
		setCheckboxState();
		campoNumeroAp.setText(selectedModel.getNumAP());
		campoAndar.setText(selectedModel.getAndar());
	}

	private void setCheckboxState() {
		if (selectedModel.isHospede())
			campoHospede.setSelected(true);
		else
			campoHospede.setSelected(false);
	}
	
	private void preparaCloseJFrameListener() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("Salvando lista de moradores...");
				repositorioMorador.salvar(moradores);
				System.out.println("Fechando...");
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				System.out.println("Fechado!");
				System.exit(0);
			}
		});
	}
	
	private void mostraJanela() {
		this.pack();
		this.setSize(845, 740);
		this.setResizable(false);
		this.setVisible(true);		
	}
}