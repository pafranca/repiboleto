package br.org.repiboleto.ui;

import java.awt.event.ActionEvent;

import br.org.repiboleto.Morador;

public class CarregarAction extends DefaultRepiBoletoActionUI {

	public CarregarAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		cleanFields();
		if (repiBoleto.index != -1) {
			carregarDados(repiBoleto.selectedModel);
		}
	}

	private void carregarDados(final Morador morador) {
		repiBoleto.campoNome.setText(morador.getNome());
		repiBoleto.campoCpf.setText(morador.getCpf());
		repiBoleto.campoEmail.setText(morador.getEmail());
		repiBoleto.campoTelefone.setText(morador.getTelefone());
		repiBoleto.campoNumeroAp.setText(morador.getNumAP());
		repiBoleto.campoAndar.setText(morador.getAndar());
	}
}