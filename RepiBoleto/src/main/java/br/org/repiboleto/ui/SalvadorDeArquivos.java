package br.org.repiboleto.ui;

import static javax.swing.JFileChooser.APPROVE_SELECTION;
import static javax.swing.JFileChooser.CANCEL_SELECTION;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.basic.BasicFileChooserUI;
import javax.swing.text.MaskFormatter;

public class SalvadorDeArquivos extends JDialog implements ActionListener {
	private CardLayout cardLayout;
	private JPanel cardPanel, painelData;
	private JLabel labelDate;
	private JButton backButton, nextButton, closeButton;
	private JFileChooser chooser;
	private JFormattedTextField campoData;
	private FileFilter pdfFilter;
	private Calendar calendar;
	
	public SalvadorDeArquivos(JFrame frame, boolean modal) {
		super(frame, "Salvar", modal);
		setLocationRelativeTo(frame);
	}
	
	public void montaTodosCards() {
		preparaFileChooser();
		preparaFiltroArquivos();
		chooser.setControlButtonsAreShown(false);
		preparaCardLayoutCompleto();
		pack();
	}
	
	public void montaDateCard() {
		preparaCardLayoutData();
		pack();
	}

	private void preparaFileChooser() {
		chooser = new JFileChooser(System.getProperty("user.home"));
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		chooser.setDragEnabled(false);
		chooser.setDialogType(JFileChooser.SAVE_DIALOG);
		chooser.setSelectedFile(new File("Boleto.pdf"));
	}
	
	private void preparaFiltroArquivos() {
		setFileFilters();
	}
	
	private void setFileFilters() {
		chooser.resetChoosableFileFilters();
		pdfFilter = createFileFilter("Arquivo PDF", true, "pdf");
		chooser.addChoosableFileFilter(pdfFilter);
	}

	private FileFilter createFileFilter(String description,
			boolean showExtensionInDescription, String... extensions) {
		if (showExtensionInDescription) {
			description = createFileNameFilterDescriptionFromExtensions(
					description, extensions);
		}
		return new FileNameExtensionFilter(description, extensions);
	}

	private String createFileNameFilterDescriptionFromExtensions(
			String description, String[] extensions) {
		String fullDescription = (description == null) ? "(" : description
				+ " (";
		// build the description from the extension list
		fullDescription += "." + extensions[0];
		for (int i = 1; i < extensions.length; i++) {
			fullDescription += ", .";
			fullDescription += extensions[i];
		}
		fullDescription += ")";
		return fullDescription;
	}
	
	private void preparaCardLayoutCompleto() {
		setCardPanel();
		setDateChooser();
		cardPanel.add(chooser, "fileChooser");
		cardPanel.add(painelData, "data");
		cardLayout.show(cardPanel, "fileChooser");
		chooser.addActionListener(this);
		setButtonPanelCompleto();
		backButton.addActionListener(this);
		nextButton.addActionListener(this);
		closeButton.addActionListener(this);
	}

	private void preparaCardLayoutData() {
		setCardPanel();
		setDateChooser();
		cardPanel.add(painelData, "data");
		cardLayout.show(cardPanel, "data");
		setButtonPanel();
		closeButton.addActionListener(this);
	}	

	private void setCardPanel() {
		cardLayout = new CardLayout();
		cardPanel = new JPanel(cardLayout);
		getContentPane().add(cardPanel, BorderLayout.CENTER);
	}
	
	private void setDateChooser() {
		painelData = new JPanel();
		try {
			labelDate = new JLabel("Selecione a data de vencimento: ", JLabel.CENTER);
			MaskFormatter mascara = new MaskFormatter("##/##/####");
			mascara.setPlaceholderCharacter('_');
			campoData = new JFormattedTextField(mascara);
			campoData.addKeyListener(new DateTextFieldAction());
			Dimension tamanho = campoData.getPreferredSize();
			tamanho.width = 70;
			campoData.setPreferredSize(tamanho);
			painelData.add(labelDate);
			painelData.add(campoData);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void setButtonPanelCompleto() {
		JPanel buttonPanel = new JPanel();
		backButton = new JButton("< Anterior");
		nextButton = new JButton("Pr�ximo >");
		closeButton = new JButton("Fechar");

		buttonPanel.add(backButton);
		buttonPanel.add(nextButton);
		buttonPanel.add(closeButton);

		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		backButton.setEnabled(false);
		getRootPane().setDefaultButton(nextButton);
	}
	
	private void setButtonPanel() {
		JPanel buttonPanel = new JPanel();
		closeButton = new JButton("Concluir");

		buttonPanel.add(closeButton);

		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		getRootPane().setDefaultButton(closeButton);
	}
	
	public void actionPerformed(ActionEvent evt) {
		Object src = evt.getSource();
		String cmd = evt.getActionCommand();

		if (src == backButton) {
			back();
		} else if (src == nextButton) {
			FileChooserUI ui = chooser.getUI();
			if (ui instanceof BasicFileChooserUI) {
				// Workaround for bug 4528663. This is necessary to
				// pick up the contents of the file chooser text field.
				// This will trigger an APPROVE_SELECTION action.
				((BasicFileChooserUI) ui).getApproveSelectionAction()
						.actionPerformed(null);
			} else {
				next();
			}
		} else if (src == closeButton) {
			close();
		} else if (cmd == APPROVE_SELECTION) {
			next();
		} else if (cmd == CANCEL_SELECTION) {
			close();
		}
	}

	private void back() {
		backButton.setEnabled(false);
		nextButton.setEnabled(true);
		closeButton.setText("Fechar");
		cardLayout.show(cardPanel, "fileChooser");
		getRootPane().setDefaultButton(nextButton);
		chooser.requestFocus();
	}

	private void next() {
		backButton.setEnabled(true);
		nextButton.setEnabled(false);
		closeButton.setText("Concluir");
		cardLayout.show(cardPanel, "data");
		getRootPane().setDefaultButton(closeButton);
		closeButton.requestFocus();
	}

	private void close() {
		//op��o para o caso do usu�rio querer fechar a janela com a data j� preenchida
		if (closeButton.getText().equals("Concluir") && calendar != null) {
			setVisible(false);
		}
		
		//op��o para o caso do usu�rio querer fechar a janela com a data j� preenchida
		if (closeButton.getText().equals("Concluir")) {
			preenchimentoData();
			if (calendar != null) {
				setVisible(false);
			}
		}
		//op��o para o caso do usu�rio querer cancelar a janela de gera��o do boleto
		if (closeButton.getText().equals("Fechar"))
			setVisible(false);
	}

	public void dispose() {
		if (chooser != null) {
			chooser.removeActionListener(this);
			// The chooser is hidden by CardLayout on remove
			// so fix it here
			cardPanel.remove(chooser);
			chooser.setVisible(true);
		}
		super.dispose();
	}
	
	public static void main(String[] args) {
		SalvadorDeArquivos salvador = new SalvadorDeArquivos(null, true);
		salvador.montaTodosCards();
		salvador.setVisible(true);
		salvador.dispose();
	}

	public File getSelectedFile() {
		File file = null;
		if (chooser != null && chooser.getSelectedFile() != null)
			file = chooser.getSelectedFile();
		return file;
	}
	
	/**
	 * TODO - Refatorar assim que poss�vel este m�todo pois pelo que me parece
	 * ele est� infringindo a coes�o da classe.
	 * Coes�o entende-se monoresponsabilidade
	 */
	public Calendar setSelectedCalendar(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			calendar = Calendar.getInstance();
			calendar.setTime(sdf.parse(date));
		} catch(ParseException e) {
			JOptionPane.showMessageDialog(rootPane, "Campo data preenchido errado!", "Aviso", JOptionPane.ERROR_MESSAGE);
			calendar = null;
			e.printStackTrace();
		}
		return calendar;
	}
	
	public Calendar getSelectedCalendar() {
		return calendar;
	}
	
	private boolean campoDataPreenchidoCorreto() {
		boolean retorno = false;
		if (!campoData.getText().contains("_"))
			retorno = true;
		return retorno;			
	}
	
	private void preenchimentoData() {
		if (!campoDataPreenchidoCorreto()) {
			JOptionPane.showMessageDialog(rootPane, "Preencha o campo!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return;
		} else {
			setSelectedCalendar(campoData.getText());
		}
	}

	class DateTextFieldAction extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				preenchimentoData();
				if (calendar != null)
					closeButton.doClick();
			}
		}
	}
}