package br.org.repiboleto.ui;

import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public abstract class DefaultRepiBoletoActionUI implements ActionListener {
	protected RepiBoletoUI repiBoleto;
	
	public void setRepiBoleto(RepiBoletoUI repiBoleto) {
		this.repiBoleto = repiBoleto;
	}

	protected boolean checkConstrains() {
		if (repiBoleto.campoNome.getText().equals("")) {
			JOptionPane.showMessageDialog(repiBoleto, "Campo Nome em branco!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (repiBoleto.campoMatricula.getText().equals("")) {
			JOptionPane
					.showMessageDialog(
							repiBoleto,
							"Campo Matr�cula em branco!\n" +
							"Caso morador seja h�spede preencher campo com a matr�cula do estudante que o convidou.",
							"Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}

		if (repiBoleto.campoNumeroAp.getText().equals("")) {
			JOptionPane.showMessageDialog(repiBoleto, "N�mero do Ap. em branco!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (repiBoleto.campoNumeroAp.getText().equals("0")) {
			JOptionPane.showMessageDialog(repiBoleto, "N�mero do Ap. n�o pode ser zero!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (repiBoleto.campoAndar.getText().equals("0")) {
			JOptionPane.showMessageDialog(repiBoleto, "Andar n�o pode ser zero!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}

		try {
			Integer.parseInt(repiBoleto.campoNumeroAp.getText());			
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(repiBoleto, "N�mero do Ap. inv�lido!", "Aviso", JOptionPane.WARNING_MESSAGE);
			System.out.println("Erro de convers�o de dados - Campo N�mero Ap.");
			return false;
		}
		
		try {
			Integer.parseInt(repiBoleto.campoAndar.getText());			
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(repiBoleto, "Andar inv�lido!", "Aviso", JOptionPane.WARNING_MESSAGE);
			System.out.println("Erro de convers�o de dados - Campo N�mero Ap.");
			return false;
		}
		
		if (repiBoleto.campoAndar.getText().equals("")) {
			JOptionPane.showMessageDialog(repiBoleto, "Campo Andar em branco!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (repiBoleto.campoEmail.getText().equals("")) {
			JOptionPane.showMessageDialog(repiBoleto, "Campo E-mail em branco!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	protected void cleanFields() {
		repiBoleto.campoNome.setText("");
		repiBoleto.campoCpf.setText("");
		repiBoleto.campoEmail.setText("");
		repiBoleto.campoTelefone.setText("");
		repiBoleto.campoMatricula.setText("");
		if (repiBoleto.campoHospede.isSelected())
			repiBoleto.campoHospede.setSelected(false);
		repiBoleto.campoNumeroAp.setText("");
		repiBoleto.campoAndar.setText("");
	}
}
