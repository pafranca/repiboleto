package br.org.repiboleto.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.JOptionPane;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public class InserirAction extends DefaultRepiBoletoActionUI {
	
	public InserirAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (checkConstrains()) {
			repiBoleto.moradores.add(popularDados());
			Collections.sort(repiBoleto.moradores);
			repiBoleto.atm.fireTableDataChanged();
			cleanFields();
			JOptionPane.showMessageDialog(repiBoleto, "Morador inserido!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
			System.out.println("Inserindo registro no banco de dados.");
			return;
		}
		return;
		//JOptionPane.showMessageDialog(repiBoleto.janela, "Morador n�o foi inserido!", "Aviso", JOptionPane.WARNING_MESSAGE);
	}
	
	private Morador popularDados() {
		return new Morador(
				repiBoleto.campoNome.getText(), 
				repiBoleto.campoCpf.getText(),
				repiBoleto.campoEmail.getText(),
				repiBoleto.campoTelefone.getText(),
				repiBoleto.campoMatricula.getText(),
				repiBoleto.campoHospede.isSelected(),
				new Apartamento(
						Integer.parseInt(repiBoleto.campoNumeroAp.getText()),
						Integer.parseInt(repiBoleto.campoAndar.getText())));
	}
}