package br.org.repiboleto.ui;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.JOptionPane;

import br.com.caelum.stella.boleto.bancos.Itau;
import br.com.caelum.stella.boleto.exception.CriacaoBoletoException;
import br.org.repiboleto.reader.BoletoFactory;
import br.org.repiboleto.reader.GeradorDeBoleto;

public class BoletoAction extends DefaultRepiBoletoActionUI {
	
	public BoletoAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (repiBoleto.index != -1) {
			File boleto = null;
			Calendar calendar = null;
			
			SalvadorDeArquivos salvador = new SalvadorDeArquivos(repiBoleto,true);
			salvador.montaTodosCards();
			salvador.setVisible(true);
			boleto = salvador.getSelectedFile();
			calendar = salvador.getSelectedCalendar();
			salvador.dispose();
			
			if (boleto != null && calendar != null) {
				repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				FileOutputStream out = null;
				try {
					GeradorDeBoleto gerador = new GeradorDeBoleto(
							BoletoFactory.constroiBoleto(
									repiBoleto.selectedModel, new Itau(),
									calendar));
					out = new FileOutputStream(boleto);
					gerador.salvaBoletoPDF(out);
					repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(repiBoleto,
							"Boleto gerado com sucesso!", "Sucesso",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				} catch (CriacaoBoletoException e1) {
					e1.printStackTrace();
					repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(repiBoleto,
							"Erro ao gerar boleto!", "Erro",
							JOptionPane.ERROR_MESSAGE);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(repiBoleto,
							"Arquivo n�o encontrado!", "Erro",
							JOptionPane.ERROR_MESSAGE);
				} catch (IOException e1) {
					e1.printStackTrace();
					repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(repiBoleto,
							"N�o foi poss�vel salvar o arquivo!", "Erro",
							JOptionPane.ERROR_MESSAGE);
				} finally {
					try {
						out.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			} else {
				JOptionPane.showMessageDialog(repiBoleto,
						"Boleto n�o foi gerado!", "Aviso",
						JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(repiBoleto,
					"Selecione um morador!", "Sucesso",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
	}
}