package br.org.repiboleto.ui;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.JOptionPane;

import org.apache.commons.mail.EmailException;

import br.com.caelum.stella.boleto.bancos.Itau;
import br.com.caelum.stella.boleto.exception.CriacaoBoletoException;
import br.org.repiboleto.Morador;
import br.org.repiboleto.reader.BoletoFactory;
import br.org.repiboleto.reader.GeradorDeBoleto;
import br.org.repiboleto.service.EmailService;

public class TodosBoletosAction extends DefaultRepiBoletoActionUI {
	
	public TodosBoletosAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		File boleto = null;
		Calendar calendar = null;
		
		SalvadorDeArquivos salvador = new SalvadorDeArquivos(repiBoleto,true);
		salvador.montaDateCard();
		salvador.setVisible(true);
		boleto = new File(System.getProperty("java.io.tmpdir") + "/boleto.pdf");
		calendar = salvador.getSelectedCalendar();
		salvador.dispose();
		
		if (calendar != null) {
			repiBoleto.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			FileOutputStream out = null;
			EmailService emailService = null;

			try {
				for (Morador morador : repiBoleto.moradores) {
					GeradorDeBoleto gerador = new GeradorDeBoleto(
							BoletoFactory.constroiBoleto(morador, new Itau(),
									calendar));
					out = new FileOutputStream(boleto);
					gerador.salvaBoletoPDF(out);

					if (emailService == null)
						emailService = EmailService.getInstance();
					emailService.enviarEmail(morador, boleto);
				}
			} catch (CriacaoBoletoException e1) {
				e1.printStackTrace();
				repiBoleto.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(repiBoleto,
						"Erro ao gerar boleto!", "Erro",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				repiBoleto.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(repiBoleto,
						"Arquivo n�o encontrado!", "Erro",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (IOException e1) {
				e1.printStackTrace();
				repiBoleto.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(repiBoleto,
						"N�o foi poss�vel salvar o arquivo!", "Erro",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (EmailException e1) {
				e1.printStackTrace();
				repiBoleto.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(repiBoleto,
						"N�o foi poss�vel enviar e-mail!", "Erro",
						JOptionPane.ERROR_MESSAGE);
				return;
			} finally {
				try {
					out.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			repiBoleto.setCursor(Cursor
					.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			JOptionPane.showMessageDialog(repiBoleto,
					"Boletos enviados por e-mail!", "Aviso",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(repiBoleto, "Boleto n�o foi enviado por e-mail!",
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
	}
}