package br.org.repiboleto.ui;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

public class RemoverAction extends DefaultRepiBoletoActionUI {
	
	public RemoverAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (repiBoleto.index != -1) {
			repiBoleto.moradores.remove(repiBoleto.index);
			repiBoleto.atm.fireTableDataChanged();
			cleanFields();
			JOptionPane.showMessageDialog(repiBoleto,
					"Registro removido!", "Sucesso",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println("Removendo registro no banco de dados.");
			return;
		}
	}
	

}
