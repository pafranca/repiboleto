package br.org.repiboleto.ui;

import java.awt.event.ActionEvent;
import java.util.Collections;

import javax.swing.JOptionPane;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public class AtualizarAction extends DefaultRepiBoletoActionUI {
	
	public AtualizarAction(RepiBoletoUI repiBoleto) {
		super.setRepiBoleto(repiBoleto);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (repiBoleto.index != -1) {
			if (checkConstrains()) {
				atualizarDados(repiBoleto.selectedModel);
				repiBoleto.moradores.set(repiBoleto.index, repiBoleto.selectedModel);
				Collections.sort(repiBoleto.moradores);
				repiBoleto.atm.fireTableDataChanged();
				cleanFields();
				JOptionPane.showMessageDialog(repiBoleto,
						"Morador atualizado!", "Sucesso",
						JOptionPane.INFORMATION_MESSAGE);
				System.out.println("Atualizado registro no banco de dados.");
				return;
			}
		}
		return;
		//JOptionPane.showMessageDialog(repiBoleto.janela, "Morador n�o foi inserido!", "Aviso", JOptionPane.WARNING_MESSAGE);
	}
	
	private void atualizarDados(Morador morador) {
		morador.setNome(repiBoleto.campoNome.getText());
		morador.setCpf(repiBoleto.campoCpf.getText());
		morador.setEmail(repiBoleto.campoEmail.getText());
		morador.setTelefone(repiBoleto.campoTelefone.getText());
		morador.setMatricula(repiBoleto.campoMatricula.getText());
		morador.setHospede(repiBoleto.campoHospede.isSelected());
		morador.getApartamento().setNumero(Integer.parseInt(repiBoleto.campoNumeroAp.getText()));
		morador.getApartamento().setAndar(Integer.parseInt(repiBoleto.campoAndar.getText()));
	}
}