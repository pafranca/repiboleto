package br.org.repiboleto.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class RepositorioMorador {
	private XStream stream;
	
	public RepositorioMorador() {
		stream = new XStream(new DomDriver("UTF-8"));
		stream.setMode(XStream.NO_REFERENCES);
	}

	public List<Morador> carregar(Reader fonte) {
		stream.alias("morador", Morador.class);
		stream.alias("apartamento", Apartamento.class);
		return (List<Morador>) stream.fromXML(fonte);
	}
	
	public void salvar(List<Morador> listaDeMoradores) {
		if (listaDeMoradores == null || listaDeMoradores.size() == 0)
			throw new IllegalArgumentException("N�o existe nenhum morador para salvar!");
		
		stream.alias("morador", Morador.class);
		stream.alias("apartamento", Apartamento.class);
		PrintStream out = null;
		
		try {
			out = new PrintStream(new File("moradores.xml"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Arquivo n�o encontrado");
		}
		out.println(stream.toXML(listaDeMoradores));
	}
}