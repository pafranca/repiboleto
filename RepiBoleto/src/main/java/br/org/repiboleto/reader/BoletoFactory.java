package br.org.repiboleto.reader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import br.com.caelum.stella.boleto.Banco;
import br.com.caelum.stella.boleto.Boleto;
import br.com.caelum.stella.boleto.Datas;
import br.com.caelum.stella.boleto.Emissor;
import br.com.caelum.stella.boleto.Sacado;
import br.org.repiboleto.Morador;

public class BoletoFactory {
	private static Properties boletoProps;
	
	public static Boleto constroiBoleto(Morador morador, Banco banco, Calendar calendar) throws FileNotFoundException, IOException {
		if (calendar == null)
			throw new IllegalArgumentException("Data n�o pode ser nula");
		
		if (morador == null) 
			throw new IllegalArgumentException("Morador nulo");
		
		if (boletoProps == null) {
			boletoProps = new Properties();
			try {
				boletoProps.load(new FileInputStream("boleto.properties"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new FileNotFoundException(
						"Arquivo de propriedades do boleto n�o foi encontrado!"
								+ e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new IOException(
						"Erro ao carregar propriedades do boleto!"
								+ e.getMessage());
			}
		}
		Boleto boleto = null;
		Calendar hoje = Calendar.getInstance();
		Datas datas = Datas.newDatas()
				.withDocumento(hoje)
				.withProcessamento(hoje);
		
		datas.withVencimento(calendar);
		
		Emissor emissor = Emissor
				.newEmissor()
				.withCedente(boletoProps.getProperty("EMISSOR"))
				.withAgencia(Integer.parseInt(boletoProps.getProperty("AGENCIA").trim()))
				.withDigitoAgencia(
						boletoProps.getProperty("DIGITO_AGENCIA").charAt(0))
				.withContaCorrente(
						Long.parseLong(boletoProps.getProperty("CONTA_CORRENTE").trim()))
				.withNumeroConvenio(
						Long.parseLong(boletoProps.getProperty("NUMERO_CONVENIO").trim()))
				.withDigitoContaCorrente(
						boletoProps.getProperty("DIGITO_CONTA_CORRENTE").charAt(0))
				.withCarteira(Integer.parseInt(boletoProps.getProperty("CARTEIRA").trim()))
				.withNossoNumero(
						Long.parseLong(boletoProps.getProperty("NOSSO_NUMERO").trim()));
		
		Sacado sacado = Sacado
				.newSacado()
				.withNome(morador.getNome())
				.withCpf("")
				.withEndereco(
						"avenida S�o Jo�o n� 2044 ap. "
								+ morador.getApartamento().getNumero() + " "
								+ morador.getApartamento().getAndar()
								+ "� andar")
				.withBairro("Centro")
				.withCep("01211-000")
				.withCidade("S�o Paulo")
				.withUf("SP");
		
		boleto = Boleto.newBoleto()
				.withBanco(banco)
				.withDatas(datas)				
				.withDescricoes(boletoProps.getProperty("DESCRICAO").split(";"))
				.withEmissor(emissor)
				.withSacado(sacado)
				.withValorBoleto((morador.isHospede()? "70.00" : "50.00"))
				.withNumeroDoDocumento("1234")
				.withInstrucoes(boletoProps.getProperty("INSTRUCAO").split(";"))
				.withLocaisDePagamento("Ag�ncia");
		
		return boleto; 
	}
	
	public static List<Boleto> gerarBoletosParaMes(List<Morador> moradores, Banco banco, Calendar calendar) throws FileNotFoundException, IOException {
		List<Boleto> listaDeBoletos = new ArrayList<Boleto>();
		
		for (Morador morador : moradores) {
			listaDeBoletos.add(constroiBoleto(morador, banco, calendar));
		}
		
		return listaDeBoletos;
	}
}