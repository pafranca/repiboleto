package br.org.repiboleto.reader;

import java.io.IOException;
import java.io.OutputStream;

import br.com.caelum.stella.boleto.Boleto;
import br.com.caelum.stella.boleto.exception.CriacaoBoletoException;
import br.com.caelum.stella.boleto.transformer.BoletoGenerator;

public class GeradorDeBoleto {
    BoletoGenerator gerador;
    Boleto boleto;
    
    public GeradorDeBoleto(Boleto boleto) {
            this.boleto = boleto;
            this.gerador = new BoletoGenerator(this.boleto);
    }
    
    public void salvaBoletoPDF(OutputStream out) throws IOException, CriacaoBoletoException {
            out.write(gerador.toPDF());
    }
    
    public void salvaBoletoPNG(OutputStream out) throws IOException, CriacaoBoletoException {
            out.write(gerador.toPNG());
    }
}