package br.org.repiboleto;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class ApartamentoTest {

	@Test(expected=IllegalArgumentException.class)
	public void apartamentoSemNumero() {
		Apartamento ap = new Apartamento(0, 1);
		assertNotNull(ap.getNumero());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void apartamentoSemAndar() {
		Apartamento ap = new Apartamento(1, 0);
		assertNotNull(ap.getAndar());
	}
	
	@Test
	public void apartamentoEquals() {
		Apartamento ap1 = new Apartamento(1, 1);
		Apartamento ap2 = new Apartamento(1, 1);
		
		Apartamento ap3 = new Apartamento(2, 1);
		Apartamento ap4 = new Apartamento(1, 3);
		
		assertEquals(ap1, ap2);
		assertNotSame(ap3, ap4);
	}
	
	@Test
	public void apartamentoHashCode() {
		Apartamento ap1 = new Apartamento(1, 1);
		Apartamento ap2 = new Apartamento(1, 1);
		
		assertEquals(ap1.hashCode(), ap2.hashCode());
	}
	
	@Test
	public void apartamentoCompareTo() {
		Set<Apartamento> s = new TreeSet<Apartamento>();
		
		Apartamento ap1 = new Apartamento(2, 1);
		Apartamento ap2 = new Apartamento(3, 1);
		Apartamento ap3 = new Apartamento(4, 2);
		Apartamento ap4 = new Apartamento(5, 1);
		Apartamento ap5 = new Apartamento(8, 2);
		Apartamento ap6 = new Apartamento(7, 1);
		Apartamento ap7 = new Apartamento(2, 4);
		Apartamento ap8 = new Apartamento(3, 5);
		Apartamento ap9 = new Apartamento(2, 1);
		
		Collections.addAll(s, ap1, ap2, ap3, ap4, ap5, ap6, ap7, ap8);
		
		System.out.println(s);
		
		assertTrue(ap1.compareTo(ap9) == 0);
	}
}