package br.org.repiboleto;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class MoradorTest {

	@Test(expected=IllegalArgumentException.class)
	public void moradorComNomeNulo() {
		Apartamento ap = new Apartamento(1, 1);
		Morador morador = new Morador(null, "34242616880", "teste@teste.com.br", "91458763", "3456789012", false, ap);
		
		assertNotNull(morador.getNome());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void moradorComApartamentoNulo() {
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "teste@teste.com.br", "91458763", "3456789012", false, null);
		
		assertNotNull(morador.getApartamento());
	}
	
	@Test
	public void moradorEquals() {
		Morador morador1 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		Morador morador2 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		assertEquals(morador1, morador2);
		assertTrue(morador1.equals(morador2));
		assertTrue(morador2.equals(morador1));
	}
	
	@Test
	public void moradorHashCode() {
		Morador morador1 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		Morador morador2 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		System.out.println(morador1.hashCode());
		System.out.println(morador2.hashCode());
		
		assertEquals(morador1.hashCode(), morador2.hashCode());
	}
	
	@Test
	public void moradorCompareTo() {
		Set<Morador> s = new TreeSet<Morador>();
		
		Morador morador1 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		Morador morador2 = new Morador("Paulo Franca", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		Morador morador3 = new Morador("Amanda de Almeida", "34242616880",
				"teste@teste.com.br", "88217461", "3456789012", false, new Apartamento(1, 1));
		
		Collections.addAll(s, morador1, morador2, morador3);
		
		System.out.println(s);
		
		assertTrue(morador1.compareTo(morador2) == 0);
		
		Iterator<Morador> it = s.iterator();
		
		//System.out.println(it.next().getNome());
		
		assertEquals("Amanda de Almeida", it.next().getNome());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void moradorComMatriculaNula() {
		Apartamento ap = new Apartamento(1, 1);
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "teste@teste.com.br", "91458763", null, false, ap);
		
		assertNull(morador);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void moradorComEmailNulo() {
		Apartamento ap = new Apartamento(1, 1);
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "", "91458763", "54325346544", false, ap);
		
		assertEquals(morador.getEmail(), "");
		assertNull(morador);
	}
}