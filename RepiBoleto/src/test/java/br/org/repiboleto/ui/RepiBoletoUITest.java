package br.org.repiboleto.ui;

import static org.fest.swing.edt.GuiActionRunner.execute;

import org.fest.swing.annotation.RunsInEDT;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;
import org.junit.Test;

public class RepiBoletoUITest extends FestSwingJUnitTestCase {
	private FrameFixture window;
	
	protected void onSetUp() {
		window = new FrameFixture(robot(), novaTela());
		window.show();
		window.resizeHeightTo(740);
	}
	
	@RunsInEDT
	private static RepiBoletoUI novaTela() {
		return execute(new GuiQuery<RepiBoletoUI>() {
			protected RepiBoletoUI executeInEDT() throws Throwable {
				return new RepiBoletoUI();
			}
		});
	}
	
	@Test
	public void testacadastroMoradorComInformacoesPadroes() {
		RepiBoletoUI repi = (RepiBoletoUI) window.target;
		
		window.textBox("campoNome").enterText("Jos� de Almeida Jesus");
		window.textBox("campoEmail").enterText("jose.almeida@hotmail.com");
		window.textBox("campoCpf").enterText("34242616880");
		window.button("botaoInserir").click();
		//window.label().requireText("Some random text");
		
	}

}

