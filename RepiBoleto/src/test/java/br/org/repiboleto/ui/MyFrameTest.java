package br.org.repiboleto.ui;

import static org.fest.swing.edt.GuiActionRunner.execute;

import org.fest.swing.annotation.RunsInEDT;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;
import org.junit.Assert;
import org.junit.Test;

public class MyFrameTest extends FestSwingJUnitTestCase {
	private FrameFixture window;
	
	protected void onSetUp() {
		window = new FrameFixture(robot(), createNewEditor());
		window.show();
	}
	
	@RunsInEDT
	private static MyFrame createNewEditor() {
		return execute(new GuiQuery<MyFrame>() {
			protected MyFrame executeInEDT() throws Throwable {
				return new MyFrame();
			}
		});
	}
	
	@Test
	public void shouldCopyTextInLabelWhenClickingButton() {
		window.textBox("textToCopy").enterText("Some random text");
		window.button().click();
		window.label().requireText("Some random text");
	}
}