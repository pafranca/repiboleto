package br.org.repiboleto.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {
	private JLabel copiedText;
	private JTextField textToCopy, textToPaste;
	private JButton copyButton;
	
	public MyFrame() {
		montaTela();
	}
	
	public void montaTela() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		
		copiedText = new JLabel("");
		textToPaste = new JTextField();
		textToCopy = new JTextField();
		textToCopy.setName("textToCopy");
		
		JPanel painelBotao = new JPanel();
		copyButton = new JButton("Copy Text To Label");
		painelBotao.add(copyButton, BorderLayout.CENTER);
		copyButton.setPreferredSize(new Dimension(200, 35));
		
		copyButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				copiedText.setText(textToCopy.getText());
				textToCopy.setText("");
			}
		});
		
		this.add(copiedText, BorderLayout.SOUTH);
		this.add(textToCopy, BorderLayout.NORTH);
		this.add(textToPaste, BorderLayout.NORTH);
		this.add(painelBotao, BorderLayout.CENTER);
		
		this.setSize(300, 150);
		pack();
		setVisible(true);
	}
	
	public JFrame getFrame() {
		return this;
	}

	public JLabel getCopiedText() {
		return copiedText;
	}

	public JTextField getTextToCopy() {
		return textToCopy;
	}

	public JButton getCopyButton() {
		return copyButton;
	}
	
	public static void main(String[] args) {
		new MyFrame();
	}
}