package br.org.repiboleto.ui;

import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import static org.junit.Assert.*;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.junit.Before;
import org.junit.Test;

public class MyFrameTest1 {
	private MyFrame frame;
	
	@Before
	public void setUp() {
		frame = new MyFrame();
		frame.show();
	}

	@Test
	public void shouldCopyTextInLabelWhenClickingButton() throws AWTException {
		JTextField text = frame.getTextToCopy();
		JButton botao = frame.getCopyButton();
		
		Point point = text.getLocationOnScreen();
		
		Robot robot = new Robot();
		robot.setAutoWaitForIdle(true);
		robot.mouseMove(point.x, point.y);
		robot.setAutoDelay(400);
		robot.keyPress(KeyEvent.VK_0);
		robot.keyRelease(KeyEvent.VK_0);
		assertEquals("0", text.getText());
	
		point = botao.getLocationOnScreen();
		
		robot.mouseMove(point.x + 10, point.y + 10);
	    robot.mousePress(MouseEvent.BUTTON1_MASK);
	    robot.mouseRelease(MouseEvent.BUTTON1_MASK);
	    
	    assertEquals(frame.getCopiedText().getText(), "0");
	}
}
