package br.org.repiboleto.service;

import java.io.File;
import java.io.IOException;

import junit.framework.AssertionFailedError;

import org.apache.commons.mail.EmailException;
import org.junit.Test;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public class EmailServiceTest {
	
	@Test
	public void enviarEmailTest() {
		try {
			EmailService email = EmailService.getInstance();
			Morador morador = new Morador("Paulo Fran�a", "34242616880", "francaniilista@gmail.com", null, "4654564", false, new Apartamento(1, 2));
			email.enviarEmail(morador, new File("/root/BoletoTeste.pdf"));
		} catch (IOException e) {
			throw new AssertionFailedError(e.getMessage());
		} catch (EmailException e) {
			throw new AssertionFailedError(e.getMessage());
		}
	}
}
