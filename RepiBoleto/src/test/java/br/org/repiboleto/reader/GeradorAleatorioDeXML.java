package br.org.repiboleto.reader;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class GeradorAleatorioDeXML {
	public static void main(String[] args) throws IOException {
		List<Morador> moradores = new ArrayList<Morador>();
		
		Morador morador1 = new Morador("Paulo", "3451251256", "exemplo1@teste.com.br", "88552244", "3456789012", false, new Apartamento(1, 1));
		Morador morador2 = new Morador("Jose", "3451251256", "exemplo1@teste.com.br", "88552244", "3456789012", false, new Apartamento(2, 1));
		Morador morador3 = new Morador("Rodrigo", "3451251256", "exemplo1@teste.com.br", "88552244", "3456789012", false, new Apartamento(1, 2));
		Morador morador4 = new Morador("Maria", "3451251256", "exemplo1@teste.com.br", "88552244", "3456789012", false, new Apartamento(2, 2));
		Morador morador5 = new Morador("Cida", "3451251256", "exemplo1@teste.com.br", "88552244", "3456789012", false, new Apartamento(1, 3));
		
		moradores.add(morador1);
		moradores.add(morador2);
		moradores.add(morador3);
		moradores.add(morador4);
		moradores.add(morador5);
		
		XStream stream = new XStream(new DomDriver());
		stream.alias("Morador", Morador.class);
		stream.setMode(XStream.NO_REFERENCES);
		
		PrintStream out = new PrintStream(new File("moradores.xml"));
		out.println(stream.toXML(moradores));
	}
}
