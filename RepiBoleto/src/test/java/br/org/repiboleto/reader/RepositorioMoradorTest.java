package br.org.repiboleto.reader;

import static junit.framework.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.AssertionFailedError;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public class RepositorioMoradorTest {

	@Before 
	public void createOutputFile() {
		File output = new File("moradores.xml");
	}
	
	@Test
	public void salvarMorador() {
		Apartamento ap = new Apartamento(21, 2);
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "teste@teste.com.br", "91458763", "3456789012", false, ap);
		List<Morador> listaDeMorador = new ArrayList<Morador>();
		listaDeMorador.add(morador);
		
		RepositorioMorador leitor = new RepositorioMorador();
		leitor.salvar(listaDeMorador);
		
		try {
			BufferedReader file = new BufferedReader(new FileReader(new File(
					"moradores.xml")));

			StringBuffer saida = new StringBuffer();

			while (file.read() != -1)
				saida.append(file.readLine());
			
			assertTrue(saida.length() > 0);
		} catch (FileNotFoundException e) {
			throw new AssertionFailedError("Arquivo n�o encontrado");
		} catch (IOException e) {
			throw new AssertionFailedError("Erro de E/S");
		}
	}
	
	@Test
	public void carregarMoradores() {
		List<Morador> listaDeMoradores = null;
		
		RepositorioMorador leitor = new RepositorioMorador();
		
		BufferedReader file;
		try {
			file = new BufferedReader(new FileReader(new File(
					"moradores.xml")));
		} catch (FileNotFoundException e) {
			throw new AssertionFailedError("Arquivo n�o encontrado");
		}
		
		listaDeMoradores = leitor.carregar(file);
		
		assertTrue(listaDeMoradores.size() != 0);
		assertEquals("Paulo Fran�a", listaDeMoradores.get(0).getNome());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void salvarSemMorador() {
		List<Morador> listaDeMorador = new ArrayList<Morador>();
		
		RepositorioMorador leitor = new RepositorioMorador();
		leitor.salvar(listaDeMorador);
	}
	
	@AfterClass
	public static void deleteOutputFile() throws Exception {
		File file = new File("moradores.xml");
		assertTrue(file.delete());		
	}
}
