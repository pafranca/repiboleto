package br.org.repiboleto.reader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import static org.junit.Assert.*;
import org.junit.Test;

import br.com.caelum.stella.boleto.Banco;
import br.com.caelum.stella.boleto.Boleto;
import br.com.caelum.stella.boleto.bancos.BancoDoBrasil;
import br.org.repiboleto.Apartamento;
import br.org.repiboleto.Morador;

public class BoletoFactoryTest {

	@Test
	public void geracaoSimplesDeBoleto() throws FileNotFoundException, IOException {
		Apartamento ap = new Apartamento(21, 2);
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "teste@teste.com.br", "91458763", "3456789012", false, ap);
		Banco banco = new BancoDoBrasil();
		
		Calendar calendar = Calendar.getInstance();
		
		Boleto boleto = BoletoFactory.constroiBoleto(morador, banco, calendar);
		
		assertNotNull(boleto);
		assertEquals("Paulo Fran�a", boleto.getSacado().getNome());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void geracaoDeBoletoSemData() throws FileNotFoundException, IOException {
		Apartamento ap = new Apartamento(21, 2);
		Morador morador = new Morador("Paulo Fran�a", "34242616880", "teste@teste.com.br", "91458763", "3456789012", false, ap);
		Banco banco = new BancoDoBrasil();
		
		Boleto boleto = BoletoFactory.constroiBoleto(morador, banco, null);
		
		assertNotNull(boleto);
		assertNotNull("Paulo Fran�a", boleto.getDatas().getVencimento());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void geracaoDeBoletoSemMorador() throws FileNotFoundException, IOException {
		Banco banco = new BancoDoBrasil();
		
		Boleto boleto = BoletoFactory.constroiBoleto(null, banco, Calendar.getInstance());
		
		assertNotNull(boleto);
		assertNotNull("Paulo Fran�a", boleto.getSacado().getNome());
		
	}
}